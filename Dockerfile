FROM openjdk:17
# VOLUME [ "/tmp" ]
COPY target/knowledge-based-filter.jar knowledge-based-filter.jar
#COPY backend/src/main/resources/db/migrationScripts
ENTRYPOINT [ "java", "-jar", "/knowledge-based-filter.jar" ]
