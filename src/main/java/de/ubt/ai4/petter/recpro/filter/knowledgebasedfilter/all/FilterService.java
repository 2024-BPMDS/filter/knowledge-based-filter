package de.ubt.ai4.petter.recpro.filter.knowledgebasedfilter.all;

import de.ubt.ai4.petter.recpro.filter.knowledgebasedfilter.attribute.meta.service.MetaAttributesFilterService;
import de.ubt.ai4.petter.recpro.filter.knowledgebasedfilter.bpmElement.service.BpmElementFilterService;
import de.ubt.ai4.petter.recpro.filter.knowledgebasedfilter.bpmElementInstance.BmpElementInstanceFilterService;
import de.ubt.ai4.petter.recpro.filter.lib.recproapi.util.RecproDataService;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.KnowledgeBasedFilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResult;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResultItem;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class FilterService {

    private RecproDataService recproDataService;
    private BpmElementFilterService bpmElementFilterService;
    private BmpElementInstanceFilterService bmpElementInstanceFilterService;
    private MetaAttributesFilterService metaAttributesFilterService;

    public Tasklist executeFilter(KnowledgeBasedFilterInstance instance) {
        this.executeFilterByFilterInstance(instance);
        Tasklist tasklist = recproDataService.getTasklistById(instance.getTasklistId());
        Tasklist bpmElementsTasklist = executeBpmElementsFilter(instance, tasklist);
        Tasklist bpmElementInstancesTasklist = executeBpmElementIntancesFilter(instance, tasklist);
        Tasklist attributesTasklist = executeMetaAttributesFilter(instance, tasklist);

        List<Task> intersection = new ArrayList<>(bpmElementsTasklist.getTasks());
        intersection.retainAll(bpmElementInstancesTasklist.getTasks());
        intersection.retainAll(attributesTasklist.getTasks());

        bpmElementsTasklist.setTasks(intersection);
        return bpmElementsTasklist;
    }

    public KnowledgeBasedFilterInstance executeFilterByFilterInstance(KnowledgeBasedFilterInstance instance) {
        if (instance.getTasklist() == null) {
            instance.setTasklist(recproDataService.getTasklistById(instance.getTasklistId()));
        }

        List<FilterResultItem> bpmElementInstanceItems = bmpElementInstanceFilterService.setBpmElementInstances(instance);
        List<FilterResultItem> bpmElementItems = bpmElementFilterService.setBpmElements(instance);
        List<FilterResultItem> metaAttributeItems = metaAttributesFilterService.setMetaAttributes(instance);

        instance.getResult().setItems(bpmElementItems);

        this.setResultItems(instance.getResult(), bpmElementInstanceItems);
        this.setResultItems(instance.getResult(), metaAttributeItems);

        return instance;
    }

    private void setResultItems(FilterResult filterResult, List<FilterResultItem> items) {
        filterResult.getItems().forEach(item -> {
            FilterResultItem i = FilterResultItem.findByTaskId(item.getTaskId(), items);
            item.setVisible(item.isVisible() && i.isVisible());
        });
    }

    public Tasklist executeBpmElementsFilter(KnowledgeBasedFilterInstance instance) {
        Tasklist tasklist = recproDataService.getTasklistById(instance.getTasklistId());
        tasklist = bpmElementFilterService.removeByBpmElements(tasklist, instance.getElements());
        return tasklist;
    }

    public Tasklist executeMetaAttributesFilter(KnowledgeBasedFilterInstance instance) {
        Tasklist tasklist = recproDataService.getTasklistById(instance.getTasklistId());
        tasklist = metaAttributesFilterService.removeByMetaAttributes(tasklist, instance.getAttributes());
        return tasklist;
    }

    private Tasklist executeBpmElementsFilter(KnowledgeBasedFilterInstance instance, Tasklist tasklist) {
        instance.setTasklist(tasklist);
        instance.getResult().setItems(bpmElementFilterService.setBpmElements(instance));
        tasklist = bpmElementFilterService.removeByBpmElements(tasklist, instance.getElements());
        return tasklist;
    }

    private Tasklist executeBpmElementIntancesFilter(KnowledgeBasedFilterInstance instance, Tasklist tasklist) {
        tasklist = bmpElementInstanceFilterService.removeByBpmElements(tasklist, instance.getElementInstances());
        return tasklist;
    }

    private Tasklist executeMetaAttributesFilter(KnowledgeBasedFilterInstance instance, Tasklist tasklist) {
        tasklist = metaAttributesFilterService.removeByMetaAttributes(tasklist, instance.getAttributes());
        return tasklist;
    }
}
