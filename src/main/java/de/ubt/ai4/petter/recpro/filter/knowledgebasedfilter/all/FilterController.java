package de.ubt.ai4.petter.recpro.filter.knowledgebasedfilter.all;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.KnowledgeBasedFilterInstance;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/filter/knowledgeBased/default")
@AllArgsConstructor
public class FilterController {

    private FilterService filterService;

    @PostMapping("/test")
    public Tasklist test(@RequestBody KnowledgeBasedFilterInstance instance) {
        return filterService.executeBpmElementsFilter(instance);
    }

    @PostMapping("/all")
    public Tasklist all(@RequestBody KnowledgeBasedFilterInstance instance) {
        return filterService.executeFilter(instance);
    }

    @PostMapping("allInstance")
    public KnowledgeBasedFilterInstance allInstance(@RequestBody KnowledgeBasedFilterInstance instance) {
        return filterService.executeFilterByFilterInstance(instance);
    }
}
