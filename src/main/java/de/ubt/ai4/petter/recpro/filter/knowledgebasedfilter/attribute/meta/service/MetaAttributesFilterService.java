package de.ubt.ai4.petter.recpro.filter.knowledgebasedfilter.attribute.meta.service;

import de.ubt.ai4.petter.recpro.filter.lib.recproapi.util.RecproDataService;
import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.RecproAttributeInstance;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.attribute.FilterAttributeInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.attribute.FilterAttributeState;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.KnowledgeBasedFilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResultItem;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class MetaAttributesFilterService {

    private RecproDataService dataService;

    public Tasklist removeByAttributes(Tasklist tasklist, List<FilterAttributeInstance> attributeInstances) {


        return tasklist;
    }

    public Tasklist removeByMetaAttributes(Tasklist tasklist, List<FilterAttributeInstance> filterAttributeInstances) {
        List<RecproAttributeInstance> a = dataService.getAttributesByProcessInstanceIds(tasklist.getTasks().stream().map(Task::getProcessInstanceId).toList());

        List<String> trueAttributeInstances = filterAttributeInstances.stream().filter(fai -> fai.getState().equals(FilterAttributeState.TRUE)).map(FilterAttributeInstance::getAttributeId).toList();

        List<Task> tasks = new ArrayList<>();

        if (trueAttributeInstances.isEmpty()) {
            List<String> falseAttributeInstances = filterAttributeInstances.stream().filter(fai -> fai.getState().equals(FilterAttributeState.FALSE)).map(FilterAttributeInstance::getAttributeId).toList();

            tasklist.getTasks().forEach(task -> {
                if (this.checkTask(task, falseAttributeInstances, a, false) && checkProcess(task, falseAttributeInstances, a, false)) {
                    tasks.add(task);
                }
            });
        } else {
            tasklist.getTasks().forEach(task -> {
                if (this.checkTask(task, trueAttributeInstances, a, true) || checkProcess(task, trueAttributeInstances, a, true)) {
                    tasks.add(task);
                }
            });
        }

        tasklist.setTasks(tasks);

        return tasklist;
    }

    public List<FilterResultItem> setMetaAttributes(KnowledgeBasedFilterInstance instance) {
        List<RecproAttributeInstance> a = dataService.getAttributesByProcessInstanceIds(instance.getTasklist().getTasks().stream().map(Task::getProcessInstanceId).toList());

        List<String> trueAttributeInstances = instance.getAttributes().stream().filter(fai -> fai.getState().equals(FilterAttributeState.TRUE)).map(FilterAttributeInstance::getAttributeId).toList();

        List<FilterResultItem> result = new ArrayList<>();

        if (trueAttributeInstances.isEmpty()) {
            List<String> falseAttributeInstances = instance.getAttributes().stream().filter(fai -> fai.getState().equals(FilterAttributeState.FALSE)).map(FilterAttributeInstance::getAttributeId).toList();

            instance.getTasklist().getTasks().forEach(task -> {
                if (this.checkTask(task, falseAttributeInstances, a, false) && checkProcess(task, falseAttributeInstances, a, false)) {
                    result.add(new FilterResultItem(null, task.getId(), null, -1, "", true));
                } else {
                    result.add(new FilterResultItem(null, task.getId(), null, -1, "", false));
                }
            });
        } else {
            instance.getTasklist().getTasks().forEach(task -> {
                if (this.checkTask(task, trueAttributeInstances, a, true) || checkProcess(task, trueAttributeInstances, a, true)) {
                    result.add(new FilterResultItem(null, task.getId(), null, -1, "", true));
                } else {
                    result.add(new FilterResultItem(null, task.getId(), null, -1, "", false));
                }
            });
        }

        return result;
    }

    private boolean checkTask(Task task, List<String> filterAttributeInstances, List<RecproAttributeInstance> recproAttributeInstances, boolean filterTrueValues) {
        List<RecproAttributeInstance> taskContainsAttributes =
                recproAttributeInstances.stream().filter(
                        attributeInstance ->
                                attributeInstance.getRecproElementId().equals(task.getRecproElementId()) &&
                                        attributeInstance.getRecproElementInstanceId().equals(task.getRecproElementInstanceId()) &&
                                        filterAttributeInstances.contains(attributeInstance.getRecproAttributeId())
                ).toList();

        if (filterTrueValues) {
            return !taskContainsAttributes.isEmpty();
        } else {
            return taskContainsAttributes.isEmpty();
        }
    }

    private boolean checkProcess(Task task, List<String> filterAttributeInstances, List<RecproAttributeInstance> recproAttributeInstances, boolean filterTrueValues) {
        List<RecproAttributeInstance> taskProcessContainsAttributes =
                recproAttributeInstances.stream().filter(
                        attributeInstance ->
                                attributeInstance.getRecproProcessInstanceId().equals(task.getProcessInstanceId())&&
                                        attributeInstance.getRecproElementInstanceId().equals(task.getProcessInstanceId()) &&
                                        filterAttributeInstances.contains(attributeInstance.getRecproAttributeId())
                ).toList();
        if (filterTrueValues) {
            return !taskProcessContainsAttributes.isEmpty();
        } else {
            return taskProcessContainsAttributes.isEmpty();
        }
    }
}
