package de.ubt.ai4.petter.recpro.filter.knowledgebasedfilter.attribute.meta;

import de.ubt.ai4.petter.recpro.filter.knowledgebasedfilter.all.FilterService;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.KnowledgeBasedFilterInstance;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/filter/knowledgeBased/meta")
@AllArgsConstructor
public class MetaAttributesFilterController {
    private FilterService filterService;

    @PostMapping("/test")
    public Tasklist test(@RequestBody KnowledgeBasedFilterInstance instance) {
        return filterService.executeMetaAttributesFilter(instance);
    }

}
