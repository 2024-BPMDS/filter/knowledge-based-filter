package de.ubt.ai4.petter.recpro.filter.knowledgebasedfilter.bpmElement.service;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElement.FilterBpmElementInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElement.FilterBpmElementState;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.KnowledgeBasedFilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResultItem;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class BpmElementFilterService {

    public Tasklist removeByBpmElements(Tasklist tasklist, List<FilterBpmElementInstance> bpmElementInstances) {
        List<String> elements = bpmElementInstances.stream().filter(ei -> ei.getState().equals(FilterBpmElementState.TRUE)).map(FilterBpmElementInstance::getRecproElementId).toList();
        if (elements.isEmpty()) {
           List<String> falseElements = bpmElementInstances.stream().filter(ei -> ei.getState().equals(FilterBpmElementState.FALSE)).map(FilterBpmElementInstance::getRecproElementId).toList();
           tasklist.setTasks(
                    tasklist.getTasks().stream().filter(task -> !falseElements.contains(task.getRecproElementId()) &&
                                !falseElements.contains(task.getProcessId())
                    ).toList()
            );
        } else {
            tasklist.setTasks(
                    tasklist.getTasks().stream().filter(task ->
                            elements.contains(task.getRecproElementId()) ||
                                    elements.contains(task.getProcessId())
                    ).toList()
            );
        }

        return tasklist;
    }

    public List<FilterResultItem> setBpmElements(KnowledgeBasedFilterInstance filterInstance) {
        List<FilterResultItem> result = new ArrayList<>();
        List<String> elements = filterInstance.getElements().stream().filter(ei -> ei.getState().equals(FilterBpmElementState.TRUE)).map(FilterBpmElementInstance::getRecproElementId).toList();
        if (elements.isEmpty()) {
            List<String> falseElements = filterInstance.getElements().stream().filter(ei -> ei.getState().equals(FilterBpmElementState.FALSE)).map(FilterBpmElementInstance::getRecproElementId).toList();
            filterInstance.getTasklist().getTasks().forEach(task -> {
                if (falseElements.contains(task.getRecproElementId()) || falseElements.contains(task.getProcessId())) {
                    result.add(new FilterResultItem(null, task.getId(), null, -1, "", false));
                } else {
                    result.add(new FilterResultItem(null, task.getId(), null, -1, "", true));
                }
            });
        } else {
            filterInstance.getTasklist().getTasks().forEach(task -> {
                if (elements.contains(task.getRecproElementId()) || elements.contains(task.getProcessId())) {
                    result.add(new FilterResultItem(null, task.getId(), null, -1, "", true));
                } else {
                    result.add(new FilterResultItem(null, task.getId(), null, -1, "", false));
                }
            });
        }
        return result;
    }
}
