package de.ubt.ai4.petter.recpro.filter.knowledgebasedfilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.ubt.ai4.petter.recpro.filter.knowledgebasedfilter", "de.ubt.ai4.petter.recpro.filter.lib.recproapi"})
public class KnowledgeBasedFilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(KnowledgeBasedFilterApplication.class, args);
	}

}
