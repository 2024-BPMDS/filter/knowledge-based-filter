package de.ubt.ai4.petter.recpro.filter.knowledgebasedfilter.bpmElementInstance;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElementInstance.FilterBpmElementInstanceInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElementInstance.FilterBpmElementInstanceState;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.KnowledgeBasedFilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResultItem;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BmpElementInstanceFilterService {
    public Tasklist removeByBpmElements(Tasklist tasklist, List<FilterBpmElementInstanceInstance> bpmElementInstances) {
        List<String> elements = bpmElementInstances.stream().filter(ei -> ei.getState().equals(FilterBpmElementInstanceState.TRUE)).map(FilterBpmElementInstanceInstance::getRecproElementInstanceId).toList();
        if (elements.isEmpty()) {
            List<String> falseElements = bpmElementInstances.stream().filter(ei -> ei.getState().equals(FilterBpmElementInstanceState.FALSE)).map(FilterBpmElementInstanceInstance::getRecproElementInstanceId).toList();
            tasklist.setTasks(
                    tasklist.getTasks().stream().filter(task -> !falseElements.contains(task.getProcessInstanceId()) &&
                            !falseElements.contains(task.getRecproElementInstanceId())
                    ).toList()
            );
        } else {
            tasklist.setTasks(
                    tasklist.getTasks().stream().filter(task ->
                            elements.contains(task.getRecproElementInstanceId()) ||
                                    elements.contains(task.getProcessInstanceId())
                    ).toList()
            );
        }

        return tasklist;
    }

    public List<FilterResultItem> setBpmElementInstances(KnowledgeBasedFilterInstance instance) {
        List<FilterResultItem> result = new ArrayList<>();

        List<String> elements = instance.getElementInstances().stream().filter(ei -> ei.getState().equals(FilterBpmElementInstanceState.TRUE)).map(FilterBpmElementInstanceInstance::getRecproElementInstanceId).toList();
        if (elements.isEmpty()) {
            List<String> falseElements = instance.getElementInstances().stream().filter(ei -> ei.getState().equals(FilterBpmElementInstanceState.FALSE)).map(FilterBpmElementInstanceInstance::getRecproElementInstanceId).toList();
            instance.getTasklist().getTasks().forEach(task -> {
                if (falseElements.contains(task.getRecproElementInstanceId()) || falseElements.contains(task.getProcessInstanceId())) {
                    result.add(new FilterResultItem(null, task.getId(), null, -1, "", false));
                } else {
                    result.add(new FilterResultItem(null, task.getId(), null, -1, "", true));
                }
            });
        } else {
            instance.getTasklist().getTasks().forEach(task -> {
                if (elements.contains(task.getProcessInstanceId()) || elements.contains(task.getRecproElementInstanceId())) {
                    result.add(new FilterResultItem(null, task.getId(), null, -1, "", true));
                } else {
                    result.add(new FilterResultItem(null, task.getId(), null, -1, "", false));
                }
            });
        }

        return result;
    }

}
